package jgil.dev.androidchallenge

import android.app.Application
import android.content.Context
import jgil.dev.androidchallenge.di.container.AppContainer

class AndroidChallenge  : Application() {

    /*
        Initialized class AppContainer which have all my manual dependencies
     */
    var appContainer = AppContainer()

    override fun onCreate() {
        super.onCreate()
        appContainer.build(this)

    }
}