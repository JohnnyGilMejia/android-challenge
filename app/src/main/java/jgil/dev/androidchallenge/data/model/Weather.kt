package jgil.dev.androidchallenge.data.model

import com.google.gson.annotations.SerializedName

/*
I have created all the fields nullable because we can have the case that there are some fields that to no exists on the json
 */

data class WeatherObject(
    @SerializedName("currently")
    var _currently: Currently?,
    @SerializedName("latitude")
    var latitude: Double?,
    @SerializedName("longitude")
    var longitude: Double?,
    @SerializedName("timezone")
    var timezone: String?
)

data class Currently(
    @SerializedName("apparentTemperature")
    var apparentTemperature: Double?,
    @SerializedName("cloudCover")
    var cloudCover: Double?,
    @SerializedName("dewPoint")
    var dewPoint: Double?,
    @SerializedName("humidity")
    var humidity: Double?,
    @SerializedName("icon")
    var icon: String?,
    @SerializedName("ozone")
    var ozone: Double?,
    @SerializedName("precipIntensity")
    var precipIntensity: Double?,
    @SerializedName("precipProbability")
    var precipProbability: Double?,
    @SerializedName("pressure")
    var pressure: Double?,
    @SerializedName("summary")
    var summary: String?,
    @SerializedName("temperature")
    var temperature: Double?,
    @SerializedName("time")
    var time: Int?,
    @SerializedName("uvIndex")
    var uvIndex: Double?,
    @SerializedName("visibility")
    var visibility: Double?,
    @SerializedName("windBearing")
    var windBearing: Double?,
    @SerializedName("windGust")
    var windGust: Double?,
    @SerializedName("windSpeed")
    var windSpeed: Double?
)