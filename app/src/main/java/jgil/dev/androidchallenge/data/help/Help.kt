package jgil.dev.androidchallenge.data.help

import jgil.dev.androidchallenge.data.model.WeatherObject

sealed class MyResult<out E, out V> {

    object Loading : MyResult<Nothing, Nothing>()
    data class Value<out V>(val value: V) : MyResult<Nothing, V>()
    data class Error<out E>(val error: E) : MyResult<E, Nothing>()

    companion object Factory{
        //higher order functions take functions as parameters or return a function
        //Kotlin has function types name: () -> V
        inline fun <V> build(function: () -> V): MyResult<Exception, V> =
            try {
                Value(function.invoke())
            } catch (e: java.lang.Exception) {
                Error(e)
            }

        fun buildLoading(): MyResult<Exception, Nothing> {
            return Loading
        }
    }
}

sealed class MyResponse<out T>{

    data class Value<out WeatherObject>(val item : WeatherObject) : MyResponse<WeatherObject>()
    data class Error(val isNetworkError: Boolean,
    val message : String,
    val code: String?,
    val cause: Exception?): MyResponse<Nothing>()
}
