package jgil.dev.androidchallenge.data.model

data class CacheTimesInSeconds(val weatherCache: Long)
