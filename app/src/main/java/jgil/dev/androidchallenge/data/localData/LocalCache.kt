package jgil.dev.androidchallenge.data.localData

import android.content.Context
import com.google.gson.Gson

class LocalCache(context: Context) {

    private val sharedPref = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    fun provideCacheItem(key: String, validTime: Long): CacheItem?{
        return getCacheItem(key, validTime)
    }

    fun getCacheItem(key: String, validTime: Long): CacheItem? {
        val itemString = sharedPref.getString(key, null)
        val item = Gson().fromJson(itemString, CacheItem::class.java)
        return if ( item != null && ((System.currentTimeMillis() - item.time) / 1000) <= validTime) item else null
    }

    fun getCacheItem(key: String): CacheItem? {
        val itemString = sharedPref.getString(key, null)
        return Gson().fromJson(itemString, CacheItem::class.java)
    }

    fun invalidateKey(key: String) {
        with(sharedPref.edit()) {
            putString(key, null)
            commit() //Used to prevent race condition when using apply
        }
    }

    fun saveCacheItem(key: String, value: String) {
        val item =
            CacheItem(value, System.currentTimeMillis())
        with(sharedPref.edit()) {
            putString(key, Gson().toJson(item))
            commit()//Used to prevent race condition when using apply
        }
    }


    companion object {
        private const val fileName = "cityAppLocalCache"
    }
}


data class CacheItem(val item: String, val time: Long){
    fun isValid(validTime: Long): Boolean{
        return ((System.currentTimeMillis() - time) / 1000) <= validTime
    }
}