package jgil.dev.androidchallenge.di.location

import android.content.Context
import android.location.Location
import com.google.android.gms.location.LocationServices

class CurrentLocationProvider(context: Context?,
                              private val reqInterval: Long,
                              private val reqFastestInterval: Long,
                              private val callback: LocationProviders) {

    private val fusedLocationClient = context?.let { LocationServices.getFusedLocationProviderClient(it) }

    /*
    Get last known location is loc is not null
     */
    fun getLastKnownLocation() {
        try {
            fusedLocationClient?.lastLocation?.addOnSuccessListener { loc: Location? ->
                if (loc != null) {
                    callback.postLocation(loc)
                }
            }
        } catch (ex: SecurityException) {
        }

    }
}

interface LocationProviders {

    fun postLocation( location: Location)
}