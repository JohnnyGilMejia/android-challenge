package jgil.dev.androidchallenge.di.container

import jgil.dev.androidchallenge.ui.main.iterator.MainMVPIterator
import jgil.dev.androidchallenge.ui.main.presenter.MainPresenter
import jgil.dev.androidchallenge.ui.main.presenter.MainPresenterMVP
import jgil.dev.androidchallenge.ui.main.view.MainView

interface Factory<T> {
    fun create(): T
}

class MainPresenterFactory(private val interator: MainMVPIterator) : Factory<MainPresenter<MainView, MainMVPIterator>>{
    override fun create(): MainPresenter<MainView, MainMVPIterator> = MainPresenter(iteratol = interator)
}