package jgil.dev.androidchallenge.di.network

import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull

class OkHttpRequest(client : OkHttpClient) {

    private var client = OkHttpClient()

    init {
        this.client = client
    }

    companion object {
        val JSON = "application/json; charset=utf-8".toMediaTypeOrNull()
    }

    fun GET(url: String): Call {
        val request = Request.Builder()
            .url(url)
            .build()

        val call = client.newCall(request)
       // call.enqueue(callback)
        return call
    }

    fun POST(url: String, parameters: HashMap<String, String>, callback: Callback): Call {
        val builder = FormBody.Builder()
        val it = parameters.entries.iterator()
        while (it.hasNext()) {
            val pair = it.next() as Map.Entry<*, *>
            builder.add(pair.key.toString(), pair.value.toString())
        }

        val formBody = builder.build()
        val request = Request.Builder()
            .url(url)
            .post(formBody)
            .build()


        val call = client.newCall(request)
        call.enqueue(callback)
        return call
    }
}