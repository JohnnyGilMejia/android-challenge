package jgil.dev.androidchallenge.di.container

import android.content.Context
import jgil.dev.androidchallenge.data.localData.LocalCache
import jgil.dev.androidchallenge.di.network.OkHttpRequest
import jgil.dev.androidchallenge.di.network.remoteConfig.RemoteConfigExternalSource
import jgil.dev.androidchallenge.ui.main.iterator.MainIterator
import okhttp3.OkHttpClient

class AppContainer {

    private lateinit var context: Context
    private lateinit var remoteConfigExternalSource: RemoteConfigExternalSource
    private var client = OkHttpClient()

    fun build(cxt: Context){
        context = cxt
        remoteConfigExternalSource = RemoteConfigExternalSource()
        remoteConfigExternalSource.initialize() //Right now the is nothing to initialize, this is only an example
    }

    internal fun provideMainInterator() : MainIterator {
        return MainIterator(LocalCache(context), remoteConfigExternalSource.getCacheTimes(), OkHttpRequest(client))
    }

    fun provideRemoteConfig() : RemoteConfigExternalSource = remoteConfigExternalSource

    var mainContainer: MainContainer? = null

}