package jgil.dev.androidchallenge.di.network.remoteConfig

import jgil.dev.androidchallenge.data.model.CacheTimesInSeconds

interface RemoteConfigProvider {

    fun initialize()
    fun getCacheTimes(): CacheTimesInSeconds
    fun getGPSResquestInterval() : Long
    fun getGPSRequestFastestInterval() : Long

}