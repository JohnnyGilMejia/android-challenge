package jgil.dev.androidchallenge.di.container

import jgil.dev.androidchallenge.ui.main.iterator.MainMVPIterator
import jgil.dev.androidchallenge.ui.main.presenter.MainPresenterMVP
import jgil.dev.androidchallenge.ui.main.view.MainView

class MainContainer(interator: MainMVPIterator) {

    val mainPresenterFactory = MainPresenterFactory(interator)

}
