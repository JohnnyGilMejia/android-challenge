package jgil.dev.androidchallenge.di.network.remoteConfig

import jgil.dev.androidchallenge.data.model.CacheTimesInSeconds

class RemoteConfigExternalSource : RemoteConfigProvider {

    /*
    No going to implement this method, but is here in case we have a provider of remote configurations of the project,
    things such as the cache time of some stuff
     */
    override fun initialize() {
    }

    /*
    Ideally this method first should try to get the value from an external source,
    if the external source is empty or is null we return the default value.

    For this example, we are only going to return the default value
     */
    override fun getCacheTimes(): CacheTimesInSeconds {
        return DEFAULT_CACHE_TIMES_VALUE
    }

    override fun getGPSResquestInterval(): Long {
        return DEFAULT_GPS_REQUEST_INTERVAL
    }

    override fun getGPSRequestFastestInterval(): Long {
        return DEFAULT_GPS_FASTEST_INTERVAL
    }

    companion object{
        private const val min = 60L
        private const val hour = 60 * min

        private const val weather = 10 * min

        private const val gpsRequestInterval = 10000L
        private const val gpsRequestFastestInterval = 5000L


        //Default
        private val DEFAULT_CACHE_TIMES_VALUE = CacheTimesInSeconds(weather)
        private const val DEFAULT_GPS_REQUEST_INTERVAL = gpsRequestInterval
        private const val DEFAULT_GPS_FASTEST_INTERVAL = gpsRequestFastestInterval


        /*
        Right now we are no using the keys,
        but still have it here in case in the future we wants to map the values with an server response, or firebase remote config
        */

        //Keys
        private const val KEY_WEATHER_CACHE = "key_weather_cache"
        private const val KEY_GPS_REQUEST_INTERVAL = "gps_request_interval"
        private const val KEY_GPS_QUEST_FASTEST_INTERVAL = "gps_request_fastest_interval"
    }
}