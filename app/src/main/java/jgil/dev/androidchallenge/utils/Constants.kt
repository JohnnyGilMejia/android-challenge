package jgil.dev.androidchallenge.utils

const val baseUrl = "https://api.darksky.net/forecast/2bb07c3bece89caf533ac9a5d23d8417/"
const val KEY_TIMEZONE = "_timezone"
const val KEY_COORDINATES = "_coordinates"
const val KEY_SUMMARY = "_summary"
const val KEY_TEMPERATURE = "_temperature"
const val KEY_LL_WEATHER_VIEW_VISIBILITY = "_keyLLWeatherViewVisibility"
const val KEY_LL_WEATHER_ERROR_VISIBILITY = "_key_LlWeatherErrorVisibility"
