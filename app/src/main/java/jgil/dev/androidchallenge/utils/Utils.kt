package jgil.dev.androidchallenge.utils

import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import java.io.IOException

fun<T> Call.enqueue(callback: CallBackKt.() -> Unit) {
    val callBackKt = CallBackKt()
    callback.invoke(callBackKt)
    this.enqueue(callBackKt)
}

class CallBackKt: Callback {


    var onResponse: ((Response) -> Unit)? = null
    var onFailure: ((t: Throwable?) -> Unit)? = null

    override fun onFailure(call: Call, e: IOException) {
        onFailure?.invoke(e)
    }

    override fun onResponse(call: Call, response: Response) {
        onResponse?.invoke(response)
    }
}