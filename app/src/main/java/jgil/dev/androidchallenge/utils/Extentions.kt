package jgil.dev.androidchallenge.utils

import jgil.dev.androidchallenge.data.help.MyResponse
import jgil.dev.androidchallenge.data.model.WeatherObject

fun WeatherObject.ParseToResponse(weatherObject: WeatherObject) : MyResponse<WeatherObject>{
    return MyResponse.Value(weatherObject)
}
