package jgil.dev.androidchallenge.communications

import jgil.dev.androidchallenge.data.help.MyResponse
import jgil.dev.androidchallenge.data.model.WeatherObject

interface ApiCallback {

    interface WeatherCallback{
        fun onSuccess(weatherObject: WeatherObject)
        fun onFailure(myResponse: MyResponse.Error)
    }

}