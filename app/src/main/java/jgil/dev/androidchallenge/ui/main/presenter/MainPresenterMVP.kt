package jgil.dev.androidchallenge.ui.main.presenter

import jgil.dev.androidchallenge.ui.base.presenter.MVPPresenter
import jgil.dev.androidchallenge.ui.main.iterator.MainMVPIterator
import jgil.dev.androidchallenge.ui.main.view.MainView

interface MainPresenterMVP<V : MainView, I: MainMVPIterator> : MVPPresenter<V,I> {

    fun getWeather(lonnlag: String)
}