package jgil.dev.androidchallenge.ui.base.view

interface MVPView {

    fun showProgress()

    fun hideProgress()

}