package jgil.dev.androidchallenge.ui.main.presenter

import jgil.dev.androidchallenge.communications.ApiCallback
import jgil.dev.androidchallenge.data.help.MyResponse
import jgil.dev.androidchallenge.data.model.WeatherObject
import jgil.dev.androidchallenge.ui.base.presenter.BasePresenter
import jgil.dev.androidchallenge.ui.main.iterator.MainMVPIterator
import jgil.dev.androidchallenge.ui.main.view.MainView


class MainPresenter<V : MainView, I: MainMVPIterator> internal constructor(iteratol : I) : BasePresenter<V,I>(iteratol),
MainPresenterMVP<V,I>, ApiCallback.WeatherCallback{

    override fun getWeather(lonnlag: String) {
        showWhenGetWeatherIsLoading()
        hideWhenGetWeatherIsLoading()

        interactor?.getWeather(lonnlag,this, true)
    }

    override fun onSuccess(weatherObject: WeatherObject) {
        getView()?.let {
            it.hideProgress()
            it.showWeatherLayout()
            it.weatherLoaded(weatherObject)
        }
    }

    override fun onFailure(myResponse: MyResponse.Error) {
        getView()?.let {
            it.hideProgress()
            it.showError()
        }
    }

    private fun showWhenGetWeatherIsLoading(){
        getView()?.showProgress()
    }

    private fun hideWhenGetWeatherIsLoading(){
        getView()?.let {
            it.hideError()
            it.hideWeatherLayout()
        }
    }
}