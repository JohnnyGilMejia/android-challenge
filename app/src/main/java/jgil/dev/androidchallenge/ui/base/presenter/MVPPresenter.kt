package jgil.dev.androidchallenge.ui.base.presenter

import jgil.dev.androidchallenge.ui.base.iterator.MVPIterator
import jgil.dev.androidchallenge.ui.base.view.MVPView

interface MVPPresenter<V : MVPView, I : MVPIterator> {

    fun onAttach(view: V?)

    fun onDetach()

    fun getView(): V?

}