package jgil.dev.androidchallenge.ui.main.view

import jgil.dev.androidchallenge.data.model.WeatherObject
import jgil.dev.androidchallenge.ui.base.view.MVPView

interface MainView : MVPView {

    fun weatherLoaded(weatherObject: WeatherObject)
    fun showWeatherLayout()
    fun hideWeatherLayout()
    fun showError()
    fun hideError()

}