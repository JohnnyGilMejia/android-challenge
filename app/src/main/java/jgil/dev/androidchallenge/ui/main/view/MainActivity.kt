package jgil.dev.androidchallenge.ui.main.view

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.Toast
import jgil.dev.androidchallenge.R
import jgil.dev.androidchallenge.data.model.WeatherObject
import jgil.dev.androidchallenge.di.container.MainContainer
import jgil.dev.androidchallenge.di.network.remoteConfig.RemoteConfigExternalSource
import jgil.dev.androidchallenge.di.location.CurrentLocationProvider
import jgil.dev.androidchallenge.di.location.LocationProviders
import jgil.dev.androidchallenge.ui.base.view.BaseActivity
import jgil.dev.androidchallenge.ui.main.iterator.MainMVPIterator
import jgil.dev.androidchallenge.ui.main.presenter.MainPresenter
import jgil.dev.androidchallenge.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.roundToInt

class MainActivity : BaseActivity(), MainView ,LocationProviders{


    lateinit var presenter : MainPresenter<MainView,MainMVPIterator>
    lateinit var locationProvider: CurrentLocationProvider
    lateinit var remoteConfig: RemoteConfigExternalSource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        injectDependencies()
        btnOnCliks()

        restoreWeatherStateIfExists(savedInstanceState)

        presenter.onAttach(this)
    }

    private fun restoreWeatherStateIfExists(savedInstanceState: Bundle?)
    {
        savedInstanceState?.let {
            tvWeatherCity.text = it.getString(KEY_TIMEZONE)
            tvWeatherLatitudLongitude.text = it.getString(KEY_COORDINATES)
            tvWeatherTemperature.text = it.getString(KEY_TEMPERATURE)
            tvWeatherSummary.text = it.getString(KEY_SUMMARY)

            llWeatherInfoLayout.visibility = it.getInt(KEY_LL_WEATHER_VIEW_VISIBILITY)
            tvWeatherError.visibility = it.getInt(KEY_LL_WEATHER_ERROR_VISIBILITY)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {

        outState.run {
            putString(KEY_TIMEZONE,tvWeatherCity.text.toString())
            putString(KEY_COORDINATES,tvWeatherLatitudLongitude.text.toString())
            putString(KEY_TEMPERATURE,tvWeatherTemperature.text.toString())
            putString(KEY_SUMMARY,tvWeatherSummary.text.toString())
            putInt(KEY_LL_WEATHER_VIEW_VISIBILITY,llWeatherInfoLayout.visibility)
            putInt(KEY_LL_WEATHER_ERROR_VISIBILITY,tvWeatherError.visibility)

        }
        super.onSaveInstanceState(outState)
    }



    /*
    manually injects all the dependencies necessary for this module to work
     */
    private fun injectDependencies(){
        appContainer.mainContainer = MainContainer(appContainer.provideMainInterator())
        presenter = (appContainer.mainContainer as MainContainer).mainPresenterFactory.create()

        remoteConfig = appContainer.provideRemoteConfig()

        locationProvider = CurrentLocationProvider(this,remoteConfig.getGPSResquestInterval(),remoteConfig.getGPSRequestFastestInterval(),this)

    }

    private fun btnOnCliks(){
        btnGetWeather.setOnClickListener {
            locationProvider.getLastKnownLocation()
        }
    }

    override fun showProgress() {
        progressBarWeather.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBarWeather.visibility = View.GONE
    }

    override fun weatherLoaded(weatherObject: WeatherObject)
    {
            weatherObject.timezone?.let {
                tvWeatherCity.text = it
            }

            weatherObject._currently?.let {

                it.summary?.let {summary -> tvWeatherSummary.text = summary }

                it.temperature?.let { temperature -> tvWeatherTemperature.text = getString(R.string.weather_display_temperature,temperature.roundToInt()) }
            }

            if(weatherObject.latitude != null && weatherObject.longitude != null)
            {
                tvWeatherLatitudLongitude.text = getString(R.string.weather_display_coordinates,weatherObject.latitude,weatherObject.longitude)

            }
    }

    override fun showWeatherLayout() {
        llWeatherInfoLayout.visibility = View.VISIBLE

    }

    override fun hideWeatherLayout() {
        llWeatherInfoLayout.visibility = View.GONE

    }

    override fun showError() {
         tvWeatherError.visibility = View.VISIBLE

    }

    override fun hideError() {
        tvWeatherError.visibility = View.GONE
    }


    override fun onResume() {
        super.onResume()
        val pending = hasPermissionNotBeenGranted(Manifest.permission.ACCESS_FINE_LOCATION)
        if (pending) {
            requestAllPermissions()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
        appContainer.mainContainer = null
    }

    override fun postLocation(location: Location) {
        presenter.getWeather("${location.latitude},${location.longitude}")
        Toast.makeText(this,"Location: ${location.latitude},${location.longitude}",Toast.LENGTH_LONG).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            requestAllPermissionsCode -> {
                //Check if GPS was requested and granted
                val index = permissions.indexOf(Manifest.permission.ACCESS_FINE_LOCATION)
                if (index >= 0 && grantResults[index] == PackageManager.PERMISSION_GRANTED) {

                }
            }
        }
    }
}
