package jgil.dev.androidchallenge.ui.base.view

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import jgil.dev.androidchallenge.AndroidChallenge
import jgil.dev.androidchallenge.di.container.AppContainer

abstract class BaseActivity: AppCompatActivity(), MVPView{

    protected lateinit var appContainer: AppContainer
    protected val requestAllPermissionsCode = 9001

    private var permissionsToRequest = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appContainer = (application as AndroidChallenge).appContainer

    }

    //------ Permissions ---------------------------------------------------------------------------

    fun hasPermissionNotBeenGranted(permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(this, permission)
            != PackageManager.PERMISSION_GRANTED) {
            if (!permissionsToRequest.contains(permission)) { //In case its called twice
                permissionsToRequest.add(permission)
            }
            return true
        }
        return false
    }

    fun hasPermissionBeenGranted(permission: String): Boolean = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED

    fun addPermissionToRequestList(permission: String) = permissionsToRequest.add(permission)

    fun clearPermissionToRequestList() = permissionsToRequest.clear()

    fun requestAllPermissions() {
        if (permissionsToRequest.size > 0) {
            ActivityCompat.requestPermissions(this,
                permissionsToRequest.toTypedArray(),
                requestAllPermissionsCode)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            requestAllPermissionsCode -> {
                val deniedPermissions = ArrayList<String>()
                val grantedPermissions = ArrayList<String>()
                permissions.forEachIndexed { index, s ->
                    if (grantResults[index] == PackageManager.PERMISSION_GRANTED){
                        grantedPermissions.add(permissions[index])
                    }
                    else {
                        deniedPermissions.add(permissions[index])
                    }
                }

                if (deniedPermissions.isNotEmpty()) onPermissionsDenied(deniedPermissions)
                if (grantedPermissions.isNotEmpty()) onPermissionsGranted(grantedPermissions)
            }
            else -> {

            }
        }
    }

    open fun onPermissionsGranted(permissions: List<String>){}

    open fun onPermissionsDenied(permissions: List<String>){}

    //----------------------------------------------------------------------------------------------
}