package jgil.dev.androidchallenge.ui.main.iterator

import jgil.dev.androidchallenge.communications.ApiCallback
import jgil.dev.androidchallenge.ui.base.iterator.MVPIterator

interface MainMVPIterator : MVPIterator {

   //if we wants to take into account cache items, we put avoidCacheItem variable on false
   fun  getWeather(laglong: String, callback: ApiCallback.WeatherCallback?, avoidCacheItem : Boolean)
}