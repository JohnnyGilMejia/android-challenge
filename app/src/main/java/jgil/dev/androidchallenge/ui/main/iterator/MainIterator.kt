package jgil.dev.androidchallenge.ui.main.iterator

import android.os.Handler
import com.google.gson.Gson
import jgil.dev.androidchallenge.communications.ApiCallback
import jgil.dev.androidchallenge.data.help.MyResponse
import jgil.dev.androidchallenge.data.localData.LocalCache
import jgil.dev.androidchallenge.data.model.WeatherObject
import jgil.dev.androidchallenge.ui.base.iterator.BaseIterator
import jgil.dev.androidchallenge.utils.baseUrl
import jgil.dev.androidchallenge.utils.enqueue
import org.json.JSONObject
import android.os.Looper.getMainLooper
import jgil.dev.androidchallenge.data.model.CacheTimesInSeconds
import jgil.dev.androidchallenge.di.network.OkHttpRequest


class MainIterator internal constructor(private val localCache: LocalCache,private val times: CacheTimesInSeconds, private val request: OkHttpRequest) : BaseIterator(), MainMVPIterator{

    override fun getWeather(
        laglong: String,
        callback: ApiCallback.WeatherCallback?,
        avoidCacheItem: Boolean
    ) {

        val cacheItem = localCache.provideCacheItem(KEY_GET_WEATHER+laglong,times.weatherCache)

        if(cacheItem == null || avoidCacheItem)
        {
            request.GET(baseUrl+laglong).enqueue<Nothing>
            {
                onResponse = {
                    val mainHandler = Handler(getMainLooper())

                    mainHandler.post {
                        if(it.code == 200)
                        {
                            var responseData = ""
                            it.body?.let { str ->
                                responseData = str.string()
                            }

                            try{
                                val json = JSONObject(responseData)
                                val weatherObject = Gson().fromJson(json.toString(), WeatherObject::class.java)

                                localCache.saveCacheItem(KEY_GET_WEATHER+laglong, json.toString())

                                callback?.onSuccess(weatherObject)

                            }catch (e: Exception){
                                val error = MyResponse.Error(false,it.message,it.code.toString().trim(),e)
                                callback?.onFailure(error)
                            }

                        }else
                        {
                            val error = MyResponse.Error(true,it.message,it.code.toString().trim(),null)
                            callback?.onFailure(error)
                        }
                    }
                }

                onFailure = {
                    val mainHandler = Handler(getMainLooper())

                    mainHandler.post {
                        it?.let {
                            val error = MyResponse.Error(true,it.message.toString(),"",null)
                            callback?.onFailure(error)
                        } ?: callback?.onFailure(MyResponse.Error(true,"","",null))
                    }
                }
            }
        }else{
            val weatherObject = Gson().fromJson(cacheItem.item, WeatherObject::class.java)
            callback?.onSuccess(weatherObject)
        }
    }


    private
    companion object {
        private const val KEY_GET_WEATHER = "getWeather"
    }

}