package jgil.dev.androidchallenge.mainTest

import jgil.dev.androidchallenge.data.help.MyResponse
import jgil.dev.androidchallenge.data.model.WeatherObject
import jgil.dev.androidchallenge.ui.main.iterator.MainIterator
import jgil.dev.androidchallenge.ui.main.presenter.MainPresenter
import jgil.dev.androidchallenge.ui.main.view.MainView
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PresenterTest {

    @Mock
    lateinit var mainView: MainView
    @Mock
    lateinit var mainIterator: MainIterator
    @Mock
    lateinit var weatherObject: WeatherObject
    @Mock
    lateinit var myErrorResponse: MyResponse.Error

    private lateinit var mainPresenter: MainPresenter<MainView,MainIterator>

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        mainPresenter = MainPresenter(mainIterator)

    }

    @Test
    fun `given getWeather method should call iterator getWeather method` (){
        mainPresenter.onAttach(mainView)
        mainPresenter.getWeather("")
        verify(mainIterator).getWeather("",mainPresenter,true)
    }

    @Test
    fun `given un onFailure method should call and if mainView IS ATTACH hideProgress SHOULD BE CALLED`(){
        mainPresenter.onAttach(mainView)
        mainPresenter.onFailure(myErrorResponse)
        verify(mainView).hideProgress()
    }

    @Test
    fun `given un onFailure method should call and if mainView IS ATTACH showError SHOULD BE CALLED`(){
        mainPresenter.onAttach(mainView)
        mainPresenter.onFailure(myErrorResponse)
        verify(mainView).showError()
    }

    @Test
    fun `given un onFailure method should call and if mainView IS NOT ATTACH hideProgress SHOULDNT BE CALLED`(){
        mainPresenter.onFailure(myErrorResponse)
        verify(mainView, never()).hideProgress()
    }

    @Test
    fun `given un onFailure method should call and if mainView IS NOT ATTACH showError SHOULDNT BE CALLED`(){
        mainPresenter.onFailure(myErrorResponse)
        verify(mainView, never()).showError()
    }

    @Test
    fun `given onSuccess method and if mainView IS ATTACH to the presenter showProgress SHOULD be called`()
    {
        mainPresenter.onAttach(mainView)
        mainPresenter.onSuccess(weatherObject)
        verify(mainView).hideProgress()
    }

    @Test
    fun `given onSuccess method and if mainView IS ATTACH to the presenter showWeatherLayout SHOULD be called`()
    {
        mainPresenter.onAttach(mainView)
        mainPresenter.onSuccess(weatherObject)
        verify(mainView).showWeatherLayout()
    }

    @Test
    fun `given onSuccess method and if mainView IS ATTACH to the presenter weatherLoaded SHOULD be called`()
    {
        mainPresenter.onAttach(mainView)
        mainPresenter.onSuccess(weatherObject)
        verify(mainView).weatherLoaded(weatherObject)
    }

    @Test
    fun `given onSuccess method and if mainView IS NOT ATTACH to the presenter hideProgress SHOULDNT be called`()
    {
        mainPresenter.onSuccess(weatherObject)
        verify(mainView,never()).hideProgress()
    }

    @Test
    fun `given onSuccess method and if mainView IS NOT ATTACH to the presenter showWeatherLayout SHOULDNT be called`()
    {
        mainPresenter.onSuccess(weatherObject)
        verify(mainView,never()).showWeatherLayout()
    }

    @Test
    fun `given onSuccess method and if mainView IS NOT ATTACH to the presenter weatherLoaded SHOULDNT be called`()
    {
        mainPresenter.onSuccess(weatherObject)
        verify(mainView,never()).weatherLoaded(weatherObject)
    }

}