package jgil.dev.androidchallenge.mainTest

import android.content.Context
import jgil.dev.androidchallenge.communications.ApiCallback
import jgil.dev.androidchallenge.data.localData.CacheItem
import jgil.dev.androidchallenge.data.localData.LocalCache
import jgil.dev.androidchallenge.data.model.CacheTimesInSeconds
import jgil.dev.androidchallenge.data.model.Currently
import jgil.dev.androidchallenge.data.model.WeatherObject
import jgil.dev.androidchallenge.di.network.OkHttpRequest
import jgil.dev.androidchallenge.ui.main.iterator.MainIterator
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals


@RunWith(MockitoJUnitRunner::class)
class IteratorTest {

    @Mock
    lateinit var context: Context
    @Mock
    lateinit var apiCallback: ApiCallback.WeatherCallback

    @Mock
    lateinit var okHttpRequest: OkHttpRequest

    lateinit var localCache : LocalCache

    lateinit var mainIterator: MainIterator

    lateinit var cacheItem: CacheItem

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        localCache = LocalCache(context)
    }


    @Test
    fun `given items have been cache when getWeather is called, cacheTImes should return cacheItem`(){

        val spyLocalCache = Mockito.spy(localCache)
        mainIterator = MainIterator(spyLocalCache,CacheTimesInSeconds(60L),okHttpRequest)

        cacheItem = CacheItem(strJson,60L)

        Mockito.doReturn(cacheItem).`when`(spyLocalCache).getCacheItem(KEY_GET_WEATHER,60L)

        mainIterator.getWeather("",apiCallback,false)

        assertEquals(cacheItem,spyLocalCache.getCacheItem(KEY_GET_WEATHER,60L))
    }

    @Test
    fun `given items have been cache when getWeather is called, cacheTImes should return cacheItem and call onSuccess`(){

        val spyLocalCache = Mockito.spy(localCache)
        mainIterator = MainIterator(spyLocalCache,CacheTimesInSeconds(60L),okHttpRequest)

        cacheItem = CacheItem(strJson,60L)

        Mockito.doReturn(cacheItem).`when`(spyLocalCache).getCacheItem(KEY_GET_WEATHER,60L)

        mainIterator.getWeather("",apiCallback,false)

        assertEquals(cacheItem,spyLocalCache.getCacheItem(KEY_GET_WEATHER,60L))

        verify(apiCallback).onSuccess(weatherObject)
    }


}

private val weatherObject = WeatherObject(
    Currently(83.54,0.52,72.66,0.79,"partly-cloudy-night",253.8,3.0E-4,0.02,1010.0,"Humid and Partly Cloudy",79.72,1574034451,0.0,6.156,235.0,6.33,6.33),
    18.426993,-69.9732554,"America Santo_Domingo")
private const val KEY_GET_WEATHER = "getWeather"
private const val strJson =
    "{\"latitude\":18.426993,\"longitude\":-69.9732554,\"timezone\":\"America Santo_Domingo\",\"currently\":{\"time\":1574034451,\"summary\":\"Humid and Partly Cloudy\",\"icon\":\"partly-cloudy-night\",\"precipIntensity\":3.0E-4,\"precipProbability\":0.02,\"precipType\":\"rain\",\"temperature\":79.72,\"apparentTemperature\":83.54,\"dewPoint\":72.66,\"humidity\":0.79,\"pressure\":1010,\"windSpeed\":6.33,\"windGust\":6.33,\"windBearing\":235,\"cloudCover\":0.52,\"uvIndex\":0,\"visibility\":6.156,\"ozone\":253.8} }"